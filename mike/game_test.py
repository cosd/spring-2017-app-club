from taboo_game import TabooGame

game = TabooGame()

# Log in to start
game.login()

# Retrieve cards to play with
game.play()

#Log out when finished
game.logout()
