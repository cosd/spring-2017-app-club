import requests
import json

class TabooGame():
  ######################################################################
  # Settings
  domain = '45.55.76.168'
  login_endpoint = '/taboo-api/user/login.json'
  logout_endpoint = '/taboo-api/user/logout.json'
  node_endpoint = '/taboo-api/entity_node.json'
  
  pwfile = 'password.json'

  ######################################################################
  # Data
  
  # Server connection data
  head = {'Content-Type':'application/json'}
  login_data = {'username':'','password':''}

  ## Game data settings
  # Minimum plays before game starts calculating difficulty
  min_plays_for_difficulty = 0

  # Cards
  cards = {}

  ######################################################################
  # Helper Functions

  # Log in
  def login(self, pw_file = pwfile, endpoint = domain + login_endpoint):
    with open(pw_file, 'r') as f:
      self.login_data = json.load(f)

    ld=json.dumps(self.login_data)

    l = requests.post(endpoint, headers=self.head, data=ld)

    user_data = json.loads(l.text)

    self.head['Cookie'] = user_data['session_name'] + '=' + user_data['sessid']
    self.head['X-CSRF-Token'] = user_data['token']

  # Log out
  def logout(self, endpoint = domain + logout_endpoint):
    x = requests.post(endpoint, headers=self.head)
  
  ######################################################################
  # Game functions
  def get_cards(self):
    # Retrieve cards from endpoint
    c = requests.get(self.domain + self.node_endpoint + '?parameters[type]=taboo_card&fields=title,nid,field_words_to_avoid', headers = self.head)
    cards = json.loads(c.text)
    # Process cards into local card data
    for card in cards:
      ret_card = {'title':card['title'],'id':card['nid'], 'words_to_avoid':[]}
      for word in card['field_words_to_avoid']['und']:
        ret_card['words_to_avoid'].append(word['value'])
      ret_card['difficulty'] = self.calculate_difficulty(ret_card)
      self.cards[ret_card['id']] = ret_card

  def calculate_difficulty(self, card):
    s = requests.get(self.domain + '/taboo-api/entity_node/' + card['id'] + '/nodes_field_taboo_card?fields=field_score_type,field_score_time', headers = self.head)
    scores = json.loads(s.text)
    score_count = {'correct':0, 'pass': 0, 'taboo': 0}
    try:
      for score in scores:
        t = score['field_score_type']['und'][0]['value']
        score_count[t] += 1
      print(score_count)
      score_total = score_count['correct'] + score_count['pass'] + score_count['taboo']
      if score_total > self.min_plays_for_difficulty:
        return 1 - (score_count['correct'] / score_total)
      else:
        return 0
    except:
      return 0
  def select_card(self):
    pass
  def record_score(self):
    pass
  
  ######################################################################
  # Play the game
  
  def play(self):
    pass
