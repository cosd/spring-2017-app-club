from taboo_game import TabooGame

game = TabooGame()

# Log in in to start
game.login()

# Retrieve cards to play with
game.get_cards()
print(game.cards)

#Log out when finished
game.logout()
